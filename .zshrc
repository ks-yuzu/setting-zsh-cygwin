#auto-complete
autoload -Uz compinit
compinit -u


## color
eval $(dircolors ~/dircolors.256dark)

if [ -n "$LS_COLORS" ]; then
    zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
fi


####################
#     history
####################
HISTFILE=~/.zsh_histfile
HISTSIZE=1000000
SAVEHIST=1000000

## Screenでのコマンド共有用
# シェルを横断して.zshhistoryに記録
setopt inc_append_history
# ヒストリを共有
setopt share_history

# 直前と同じコマンドをヒストリに追加しない
setopt hist_ignore_dups
# ヒストリに追加されるコマンド行が古いものと同じなら古いものを削除
setopt hist_ignore_all_dups


###################
#      alias
###################
alias ls='ls -F --show-control-chars --color=auto'
alias la='ls -a'
alias lal='ls -al --color=auto'

alias proj="cd '/cygdrive/d/Documents/Visual Studio 2013/Projects'"


# emacs 風キーバインドにする
bindkey -e

# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/yuzu/.zshrc'


PROMPT="%F{green}%n@%m%f %F{yellow}%~%f
%# "


# プロンプト
autoload -Uz vcs_info
setopt prompt_subst

#zstyle ':vcs_info:*' enable git svn
#zstyle ':vcs_info:*' max-exports 6 # formatに入る変数の最大数

zstyle ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:git:*' stagedstr "%F{yellow}!"
zstyle ':vcs_info:git:*' unstagedstr "%F{red}+"
zstyle ':vcs_info:*' formats "%F{green}%c%u[%b]%f"

zstyle ':vcs_info:*' actionformats '[%b|%a]'

precmd () { vcs_info }

RPROMPT='${vcs_info_msg_0_}'



